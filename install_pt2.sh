#! /bin/bash

echo 'Install disk?'
parted -l | grep 'Disk /' | cut -d' ' -f2 | tr -d ':'
echo -n '> '; read DISK_CHOICE

ln -sf /usr/share/zoneinfo/`curl https://ipapi.co/timezone` /etc/localtime
hwclock --systohc

sed -i 's/#en_IE.UTF-8/en_IE.UTF-8/g' /etc/locale.gen
locale-gen
echo 'LANG=en_IE.UTF-8' > /etc/locale.conf

echo 'Hostname?'
echo -n '> '; read HOSTNAME
echo $HOSTNAME > /etc/hostname

echo 'root password?'
passwd
echo 'Normal user name?'
echo -n '> '; read USERNAME
echo 'Normal user password?'
groupadd sudo
useradd -m $USERNAME -G sudo
passwd $USERNAME
sed 's/^# %sudo/%sudo/g' -i /etc/sudoers

# use systemd for init stuff
sed 's/^HOOKS/#HOOKS/g' -i /etc/mkinitcpio.conf
echo '
HOOKS=(base systemd autodetect modconf block filesystems keyboard fsck)' >> /etc/mkinitcpio.conf
mkinitcpio -P

# make swap file
echo 'Swapfile size? (GB)'
echo -n '> '; read SWAPFILE_GB_SIZE
dd if=/dev/zero of=/swapfile bs=128M count=$(( 8 * "${SWAPFILE_GB_SIZE}" ))
chmod 600 /swapfile
mkswap /swapfile
swapon /swapfile
swapon -s
echo '
# swapfile
/swapfile swap swap defaults 0 0' >> /etc/fstab

# config bootloader
ROOT_UUID=`blkid | grep "${DISK_CHOICE}"p2 | cut -d'=' -f2 | sed -E 's/"(.+)".*/\1/g'`
SWAPFILE_UUID=`findmnt -no UUID -T /swapfile`
SWAPFILE_OFFSET=`filefrag -v /swapfile | awk '$1=="0:" {print substr($4, 1, length($4)-2)}'`
efibootmgr -b 0000 -B 0000
efibootmgr --disk "${DISK_CHOICE}" --part 1 --create --label "Arch" --loader /vmlinuz-linux
--unicode "root=UUID=${ROOT_UUID} rw initrd=\intel-ucode.img initrd=\initramfs-linux.img quiet loglevel=1 intel_iommu=off vga=current rd.systemd.show_status=auto rd.udev.log_priority=1 i915.fastboot=1 nvidia_drm.modeset=1 transparent_hugepage=never rcutree.gp_init_delay=1"

# for after reboot
systemctl enable NetworkManager

exit
reboot
