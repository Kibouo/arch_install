#! /bin/bash

set -e

# enable multilib
sudo sed -i '/^#\[multilib\]/{N;s/\n#/\n/}' /etc/pacman.conf
sudo sed -i '/^#\[multilib\]/s/^#//' /etc/pacman.conf

# install aur helper
sudo pacman -Syyu --needed base-devel multilib-devel rustup
sudo pacman-key --populate archlinux
sudo pacman-key --refresh-keys
rustup toolchain install stable
rm -rf paru
git clone https://aur.archlinux.org/paru.git
cd paru
makepkg -si
cd -

# install all packages
paru --gendb
for package in `cat packages/base`; do
    set +e
    paru -S $package --noconfirm --needed
    if [[ $? -ne 0 ]]; then
        echo $package >>/tmp/pkgs_failed_2_install
    fi
    set -e
done

# install type
echo 'Computer use-case? [daily, pentest]'
echo -n '> '; read USE_CASE
bash ./${USE_CASE}_install.sh

## system config
# user home dirs
xdg-user-dirs-update
xdg-user-dirs-gtk-update
# default shell
chsh -s $(which zsh)
# add user to groups
USER=`whoami`
sudo usermod -a -G wireshark,tty,cups,uucp,input $USER
# auto timezone set
echo '#!/bin/sh
case "$2" in
    up)
        timedatectl set-timezone "$(curl --fail https://ipapi.co/timezone)"
    ;;
esac' | sudo tee /etc/NetworkManager/dispatcher.d/09-timezone >/dev/null
sudo chmod 700 /etc/NetworkManager/dispatcher.d/09-timezone
# longer sudo session timeout
echo -e "
# ask password only 1ce every 6h
Defaults\tenv_reset, timestamp_timeout=360" | sudo tee -a /etc/sudoers >/dev/null
# handle lid switch
sudo mkdir -p /etc/systemd/logind.conf.d/
echo '[Login]
HandleLidSwitchExternalPower=ignore' | sudo tee /etc/systemd/logind.conf.d/main.conf >/dev/null
# auto power up bluetooth at boot
echo '
[Policy]
AutoEnable=true' | sudo tee -a /etc/bluetooth/main.conf >/dev/null
# battery optimisations
sudo mkdir -p /etc/tlp.d
sudo touch /etc/tlp.d/01-tlp.conf
echo '
CPU_SCALING_GOVERNOR_ON_AC=performance
CPU_SCALING_GOVERNOR_ON_BAT=powersave' | sudo tee -a /etc/tlp.d/01-tlp.conf >/dev/null
# set tmp cleanup timer
sudo touch /etc/tmpfiles.d/tmp.conf
echo '
q /tmp 1777 root root 1d
q /var/tmp 1777 root root 1d' | sudo tee -a /etc/tmpfiles.d/tmp.conf >/dev/null
# allow non-priveleged ptrace attaching to process
# I.e. allow non-priv users to attach to processes with GDB
echo 'kernel.yama.ptrace_scope = 0' | sudo tee -a /etc/sysctl.d/10-ptrace.conf >/dev/null
# allow fwupd to install HW firmware updates
set +e
sudo mkdir /boot/EFI/
set -e
# configure smb
sudo mkdir /etc/samba
echo 'client min protocol = NT1' | sudo tee -a /etc/samba/smb.conf >/dev/null

# enable services
for service in `cat services`; do
    sudo systemctl enable $service
    sudo systemctl start $service
done
# disable & mask stuff for faster boot
sudo systemctl disable NetworkManager-wait-online.service
sudo systemctl mask NetworkManager-wait-online.service
# mask stuff for tlp
sudo systemctl status tlp | /usr/bin/grep active
if [[ $? -eq 0 ]]
then
    sudo systemctl mask systemd-rfkill.socket
    sudo systemctl mask systemd-rfkill.service
fi
sudo systemctl daemon-reload
# remove tty2 for ly
sudo systemctl disable getty@tty2.service
# unlock gnome-keyring when logging in
echo '
auth       optional     pam_gnome_keyring.so
session    optional     pam_gnome_keyring.so auto_start' | sudo tee -a /etc/pam.d/ly >/dev/null
# fix permission flippering of Windows drives mounted in WSL
echo '[boot]
systemd=true

[automount]
enabled = true
options = "metadata,umask=22,fmask=11"
mountFsTab = true' | sudo tee /etc/wsl.conf >/dev/null

# get dotfiles
git clone https://gitlab.com/Kibouo/dotfiles.git ~/.dotfiles/
cd ~/.dotfiles/
./deploy.sh
cd -

echo 'These packages failed to install (/tmp/pkgs_failed_2_install):'
cat /tmp/pkgs_failed_2_install
