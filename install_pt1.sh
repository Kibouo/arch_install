#! /bin/bash

ping archlinux.org -c 1
if [[ $? -ne 0 ]]; then
    echo 'Connect to internet 1st!'
    exit 1
fi

timedatectl set-ntp true

echo 'Install disk?'
parted -l | grep 'Disk /' | cut -d' ' -f2 | tr -d ':'
echo -n '> '; read DISK_CHOICE
fdisk "${DISK_CHOICE}"
mkfs.fat -F 32 "${DISK_CHOICE}"p1
mkfs.ext4 "${DISK_CHOICE}"p2

mount "${DISK_CHOICE}"p2 /mnt
mount --mkdir "${DISK_CHOICE}"p1 /mnt/boot

pacstrap /mnt base base-devel linux sudo linux-firmware nano efibootmgr parted git networkmanager
genfstab -U /mnt > /mnt/etc/fstab

cp ./install_pt2.sh /mnt/install_pt2.sh

## CHROOT ##
echo '###########################################################'
echo '# NOW DO `arch-chroot /mnt` and execute `/install_pt2.sh` #'
echo '###########################################################'
