#! /bin/bash

set -e

# packages
for package in `cat packages/graphical packages/laptop`; do
    set +e
    paru -S $package --noconfirm --needed
    if [[ $? -ne 0 ]]; then
        echo $package >>/tmp/pkgs_failed_2_install
    fi
    set -e
done

# install gdrive
opam init
opam update
opam install google-drive-ocamlfuse

set +e
mkdir ~/GoogleDrive
set -e
cp ./mount.sh ~/GoogleDrive
